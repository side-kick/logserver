app = require('express.io')();
app.http().io();

var mongo = require('mongodb');
var monk = require('monk');
var sys = require('sys');
var path = require('path');
var fs = require("fs");


var db = monk('localhost:27017/nodetest1');

// Initial web request.
app.get('/push', function(req, res) {
    // Forward to an io route.
    var collection = db.get('logcollection');

    // Submit to the DB
    req.logdata = {
        "user" : req.query.userInfo,
        "message" : req.query.message,
        "logtype" : req.query.logtype,
        "create_time" : new Date()
    };

    collection.insert(req.logdata, function (err, doc) {
        if (err) {
            // If it failed, return error
            console.log("err");
            req.io.route('hello-again');
        } else {
            // And forward to success page
            console.log("success");
            req.io.route('hello-again');
        }
    });

});


// Sends response from io route.
app.io.route('hello-again', function(req) {
    var logText = req.logdata.create_time.toDateString() +
                '['+req.logdata.logtype+'] ' + ' (' +
                req.logdata.user +
                ') ' + ' ' +
                req.logdata.message;

    req.io.broadcast('applog', logText );
    req.io.respond({status: 'success'});
});

app.get('/logview', function(req, res) {
    var collection = db.get('logcollection');

    collection.find({}, {sort: {create_time: -1}}, function(e, docs) {
        req.result = docs;
        req.io.route('prevdata');
    });
});

app.io.route('prevdata', function (req) {
    req.io.respond({result: req.result});
});

app.get('/serverlogview', function(req, res) {
    var cp = require("child_process");

    cp.exec("tail "+filename, function (err, stdout, stderr) {
        req.prevserverdata = stdout;
        console.log(req.prevserverdata);
        req.io.route('prevserverdata');
    });
});


app.io.route('prevserverdata', function (req) {
    req.io.respond({result: req.prevserverdata});
});

app.get('/test', function (req, res) {
    console.log(req);
});

// Send the client html.
app.get('/client', function(req, res) {
    res.sendfile(__dirname + '/client.html');
});

app.listen(7076);

var filename = __dirname + '/../../log/apache2/error.log';
fs.watch(filename, function(event, filename) {
    console.log("Event:", event);

    if (event == "change") {
        fs.readFile(__dirname + '/../../log/apache2/error.log', "UTF-8", function(err, data) {
            if (err) {
                console.log(err);
            }

            app.io.broadcast('serverlog', data);
        })
    }

});